import { createStore } from 'vuex'
import axios from 'axios'
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

export default createStore({
  state: {
    albumLists: [],
    photos: [],
  },
  mutations: {
    SET_ALBUM_LISTS(state, payload) {
        state.albumLists = payload;
    },
    SET_PHOTOS(state, payload) {
        state.photos = payload;    
    },
    UPDATE_LIKED_PHOTO(state, payload) {
      const selectedPhoto = state.photos.find(val => val.id === payload.id);
      const selectedPhotoIndex = state.photos.indexOf(selectedPhoto);
      selectedPhoto.isLiked = !selectedPhoto.isLiked;
      state.photos[selectedPhotoIndex] = selectedPhoto;
    },
  },
  actions: {
    // Fetch 2 albums list from a specific user (1)
    async fetchAlbumsList({ commit }) {
        try {
            const { data } = await axios.get('https://jsonplaceholder.typicode.com/albums?_limit=2?userId=1')
            commit('SET_ALBUM_LISTS', data);
        } catch (err) {
            commit('SET_ALBUM_LISTS', []);
            throw 'Unable to fetch album lists';
        }
    },
  
    // fetch the photos from a particular album clicked by the user 
    async fetchPhotosFromAlbum({ commit }, id) {
        try {
            const { data } = await axios.get(`https://jsonplaceholder.typicode.com/albums/${id}/photos?_limit=9`)
            const updatedData = data.map(obj => ({ ...obj, isLiked: false }));
            commit('SET_PHOTOS', updatedData);
        } catch (err) {
            context.commit('SET_PHOTOS', []);
            throw `Unable to fetch photos for album ${id}`;
        }
    },
    
    updateLikedPhoto({ commit }, picture) {
      commit('UPDATE_LIKED_PHOTO', picture);
    },

  },
  getters: {
    getAlbumsList: state => {
      return state.albumLists;
    },
    getPhotosFromAlbum: state => {
        return state.photos;
    }
  },
  plugins: [vuexLocal.plugin]
})
