import { mount } from '@vue/test-utils';
import { createStore } from 'vuex';
import SidebarComponent from '@/components/SidebarComponent.vue';

const store = createStore({
    state: {
      albumLists: [
        {
            userId: 1,
            id: 1,
            title: "dummy title"
        }
      ],
      photos: []
    },
    mutations: {
      SET_PHOTOS(state, payload) {
        state.photos = payload
      }
    },
    getters: {
        getAlbumsList: state => {
          return state.albumLists;
        },
    }
});

describe('SidebarComponent.vue', () => {
  it('Clicking on album fetches photos', async () => {
    const wrapper = mount(SidebarComponent, {
        global: {
          plugins: [store]
        }
    });
    await wrapper.find('li').trigger('click');
    store.commit('SET_PHOTOS', store.state.albumLists);
    expect(store.state.photos.length).toBe(1);
  })
})