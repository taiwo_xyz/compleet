import { createStore } from 'vuex';

describe('Vuex store', () => {
  it('test set album list commit in isolation', () => {
    const store = createStore({
        state: {
          albumLists: []
        },
        mutations: {
          SET_ALBUM_LISTS(state, payload) {
            state.albumLists = payload
          }
        }
    })
    const data = [
        {
          "userId": 1,
          "id": 1,
          "title": "dummy data"
        }
    ]
    store.commit('SET_ALBUM_LISTS', data);
    expect(store.state.albumLists.length).toBe(1);
  })
})